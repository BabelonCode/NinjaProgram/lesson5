﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace _002_Expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            Expression<Func<Student, bool>> isTeenAgerExpr = s => s.Age > 12 && s.Age < 20;

            //((isTeenAgerExpr as LambdaExpression).Body as BinaryExpression)

            Func<Student, bool> isTeenAger = isTeenAgerExpr.Compile();
            var st = new Student { Name = "A1", Surname = "A1yan", Age = 18 };
            if (isTeenAger(st))
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }

            Console.ReadLine();
        }
    }
}
